print('...........')

for f in 'banana' :
    if f == 'a':
        break
    print(f)
print('end')

print('...........')

sum = 0 
for i in range(101):
    sum = sum + i
print('sum = ' , sum ) 

print('---------')


for i in range(11):
    print(i)   


print('...........')


for i in range(0,101,2):
    print(i)

print('........')

for x in range(-10,10):
    print(x)
    if x == 5:
        break
else :
    print('finally finished')    
    
print('...........')

sum = 0
for i in range(1,6):
    if i == 5 :
        print (i,end= '=')
    else :
        print (i, end= '+')
    sum = sum + i
print(sum)            