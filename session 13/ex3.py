class Person :
    def __init__(self, fname , lname):
        self.firstname = fname
        self.lastname = lname


    def printname(self):
        print(self.firstname , self.lastname) 



class Student(Person):
    def __init__(self, fname , lname , std_num):
        super().__init__(fname,lname)
        self.std_num = std_num

x = Student('John' , 'Doe' , 213)
print(x.std_num)                   