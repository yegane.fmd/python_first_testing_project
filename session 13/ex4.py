class Vehicle :
    def __init__(self,brand,model):
        self.brand = brand
        self.model = model

    def move(self):
        print("Move!")

class Car(Vehicle):
    pass

class Boat(Vehicle):
    def move(self):
        print('sail')

class Plane(Vehicle):
    def move(self):
        print('fly!')

car1 = Car('test1' , 'test2')
car1.move()

boat1 = Boat('test1' , 'test2')
boat1.move()

plane1 = Plane('test1' , 'test2')
plane1.move()