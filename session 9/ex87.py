x = ("apple", "banana", "cherry")
y = list(x)
y[1] = "kiwi"
x = tuple(y)

print(x)

print('..........')

my_tuple = ('test1' ,'test 2','ali','test3')
my_list = list(my_tuple)
my_list.append('another item')
my_tuple = tuple(my_list)
print(my_tuple)

print('..........')

fruits = ("apple", "banana", "cherry")

(green, yellow, red) = fruits

print(green)
print(yellow)                   #unpacking
print(red)

print('..........')

