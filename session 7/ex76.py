def fibonacci_sequence(n):
    sequence = [0, 1]  # Initial sequence with first two numbers
    
    while len(sequence) < n:
        next_number = sequence[-1] + sequence[-2]  # Calculate the next number
        sequence.append(next_number)  # Add the next number to the sequence
    
    return sequence

# Example usage:
n_terms = 10  # Number of terms in the sequence
fib_sequence = fibonacci_sequence(n_terms)
print(fib_sequence)
