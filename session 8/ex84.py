fruits = ["apple", "banana", "cherry", "kiwi", "mango"]

# newlist=[]
# for x in fruits:
#     if x!="banana":
#         newlist.append(x)
#     else:
#             newlist.append('orange')
# print(newlist)

newlist = [x if x != "banana" else "orange" for x in fruits]
print(newlist)