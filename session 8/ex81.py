thislist = ["apple", "banana", "cherry"]
thislist[1] = "blackcurrant"
print(thislist)

print('.......')

thislist = ["apple", "banana", "cherry", "orange", "kiwi", "mango"]
thislist[1:3] = ["blackcurrant", "watermelon"]
print(thislist)

print('.......')

thislist = ["apple", "banana", "cherry"]
thislist[1:2] = ["blackcurrant", "watermelon"]
print(thislist)

print('.......')

thislist = ["apple", "banana", "cherry"]
thislist.insert(2, "watermelon")
print(thislist)

print('......')

users = []
for i in range(0,3):
    user_name = input('pls enter user name : ')
    users.append(user_name)

print(users)    

print('.......')

thislist = ["apple", "banana", "cherry"]
thislist.insert(1, "orange")
print(thislist)

print('.......')

thislist = ["apple", "banana", "cherry"]
tropical = ["mango", "pineapple", "papaya"]
thislist.extend(tropical)
print(thislist)

print('.......')

thislist = ["apple", "banana", "cherry"]
thislist.remove("banana")
print(thislist)

print('.......')

#اینجا باید یه کدی وارد کنیی ! 

