stds=['s1','s2','s3','s4','s5']
for i in range(0,len(stds),2):
    print(stds[i])

print('.......') 

fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
newlist = []

for x in fruits:
  if "a" in x:
    newlist.append(x)

print(newlist)

#short hand 

# fruits = ["apple", "banana", "cherry", "kiwi", "mango"]

# newlist = [x for x in fruits if "a" in x]

# print(newlist)

print('........')

nums = [12 , 15 , 14 , 12 , 13 ,14 , 19 , 18]

#newlist = []

# for x in nums :
#    if x > 16 :
#       newlist.append(x)
# print(newlist)

newlist = [x for x in nums if x > 16]
print(newlist)

print('.........')
