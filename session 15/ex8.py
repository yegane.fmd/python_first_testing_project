import numpy as np

# تعریف ماتریس
matrix = np.array([[2, 3], [4, 5]])

# محاسبه ماتریس معکوس
inverse_matrix = np.linalg.inv(matrix)

# چاپ ماتریس معکوس
print(inverse_matrix)
